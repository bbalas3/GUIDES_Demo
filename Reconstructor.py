import shapefile
import osgeo.ogr, osgeo.osr
from osgeo import ogr
import sys
import os
import time
import random
import networkx as nx
from networkx.classes.graph import Graph
from shutil import copyfile
import GraphGenerator

#repairs the incorrect graph by comparing it to
#a correct example/layer/graph and adding the missing edges.
#This DOES NOT REMOVE any edges from the incomplete graph, so if the water pipes
#have a network that is bigger than the road network, all its parts are preserved.
def repair(complete_nodes_layer, complete_edges_layer, incomplete_nodes_layer, incomplete_edges_layer):
    flagged_nodes = []
    new_edges = [] #keeps track of the new edges we add

    #find all the flagged nodes
    for feature in range(incomplete_nodes_layer.GetFeatureCount()):
        current_node = incomplete_nodes_layer.GetFeature(feature)
        if current_node is not None:
            if current_node.GetFieldAsInteger(3) == 1:
                temp = []
                temp.append(current_node.GetFieldAsInteger(0))
                temp.append(current_node.GetFieldAsDouble(1))
                temp.append(current_node.GetFieldAsDouble(2))
                flagged_nodes.append(temp)

    edgefeatureDefn = incomplete_edges_layer.GetLayerDefn()
    maxID_edge = incomplete_edges_layer.GetFeatureCount() +1
    already_added = [] #keeps track of which edges we've already added
    distance = 30 #distance between the nodes to connect
    for node1 in flagged_nodes:
        for node2 in flagged_nodes:
            if node1 != node2:
                if node1[2] == node2[2]: #they have the same coor_y
                    if abs(node1[1]-node2[1]) <= distance: #if they aren't too far
                        if [node1, node2] not in already_added and [node2, node1] not in already_added:
                            edge_feature = ogr.Feature(edgefeatureDefn)
                            edge = ogr.Geometry(ogr.wkbLineString)
                            edge.AddPoint(node1[1], node1[2])
                        edge.AddPoint(node2[1], node2[2])
                        edge_feature.SetField("x_start", node1[1])
                        edge_feature.SetField("y_start", node1[2])
                        edge_feature.SetField("x_end", node2[1])
                        edge_feature.SetField("y_end", node2[2])
                        edge_feature.SetGeometry(edge)
                        edge_feature.SetField("id", maxID_edge)
                        incomplete_edges_layer.CreateFeature(edge_feature)
                        maxID_edge += 1
                if node1[1] == node2[1]: #they have the same coor_x
                    if abs(node1[2]-node2[2]) <= distance: #if they aren't too far
                        if [node1, node2] not in already_added and [node2, node1] not in already_added:
                            edge_feature = ogr.Feature(edgefeatureDefn)
                            edge = ogr.Geometry(ogr.wkbLineString)
                            edge.AddPoint(node1[1], node1[2])
                            edge.AddPoint(node2[1], node2[2])
                            edge_feature.SetField("x_start", node1[1])
                            edge_feature.SetField("y_start", node1[2])
                            edge_feature.SetField("x_end", node2[1])
                            edge_feature.SetField("y_end", node2[2])
                            edge_feature.SetGeometry(edge)
                            edge_feature.SetField("id", maxID_edge)
                            incomplete_edges_layer.CreateFeature(edge_feature)
                            maxID_edge += 1
    incomplete_edges_layer.SyncToDisk()
    g = nx.read_shp(path + '/OrthogonalGraphs/Repaired/', True)
    g.to_undirected(False)
    g = GraphGenerator.clean_overlapping_edges(g)
    GraphGenerator.network_to_shp(g, path + '/OrthogonalGraphs/Repaired/')
    g = nx.read_shp(path + '/OrthogonalGraphs/Repaired/', True)
    gcopy = nx.read_shp(path + '/OrthogonalGraphs/Repaired/', True)
    g = GraphGenerator.remove_double_edges(g, gcopy)
    GraphGenerator.network_to_shp(g, path + '/OrthogonalGraphs/Repaired/')

    ##Now we want to see which new edges have been added, compared to the Incomplete shapefile
    repaired_nodes_shapefile = ogr.Open(path+'/OrthogonalGraphs/Repaired/nodes.shp', 1)
    repaired_nodes_layer = complete_nodes_shapefile.GetLayerByIndex(0)
    repaired_edges_shapefile = ogr.Open(path+'/OrthogonalGraphs/Repaired/edges.shp', 1)
    repaired_edges_layer = repaired_edges_shapefile.GetLayerByIndex(0)
    incomplete_nodes_shapefile = ogr.Open(path+'/OrthogonalGraphs/Missing_Edges/nodes.shp', 1)
    incomplete_nodes_layer = incomplete_nodes_shapefile.GetLayerByIndex(0)
    incomplete_edges_shapefile = ogr.Open(path+'/OrthogonalGraphs/Missing_Edges/edges.shp', 1)
    incomplete_edges_layer = incomplete_edges_shapefile.GetLayerByIndex(0)

    for repaired_edge in repaired_edges_layer:
        incomplete_edges_layer.ResetReading()
        temp = []
        found = False
        temp.append(repaired_edge.GetFieldAsDouble(1))
        temp.append(repaired_edge.GetFieldAsDouble(2))
        temp.append(repaired_edge.GetFieldAsDouble(3))
        temp.append(repaired_edge.GetFieldAsDouble(4))
        for incomplete_edge in incomplete_edges_layer:
            temp1 = []
            temp1.append(incomplete_edge.GetFieldAsDouble(1))
            temp1.append(incomplete_edge.GetFieldAsDouble(2))
            temp1.append(incomplete_edge.GetFieldAsDouble(3))
            temp1.append(incomplete_edge.GetFieldAsDouble(4))
            if (temp[:2] == temp1[:2] and temp[2:]==temp1[2:]) or (temp[:2] == temp1[2:] and temp[2:]==temp1[:2]):
                found = True
                pass
        if not found:
            new_edges.append(temp)

    return new_edges


def calculate_similarity(repaired_edges, edges_to_be_found):
    correct_edges_found = 0
    all_edges_found = len(repaired_edges)

    for repaired in repaired_edges:
        for complete in edges_to_be_found:
            if (repaired[:2] == complete[:2] and repaired[2:]==complete[2:]) or (repaired[:2] == complete[2:] and repaired[2:]==complete[:2]):
                correct_edges_found += 1
    print ("repaired_edges "+str(len(repaired_edges)))
    print ("edges_to_be_found "+str(len(edges_to_be_found)))
    print (correct_edges_found)
    recall = float(correct_edges_found)/float(len(edges_to_be_found))
    precision = float(correct_edges_found)/float(all_edges_found)
    print ("recall ")
    print (recall)
    print ("precision ")
    print (precision)



path = os.getcwd()

#Copy the shapefiles in the Missing_Edges folder to the Repaired folder
src_files = os.listdir(path + '/OrthogonalGraphs/Missing_Edges/')
for file_name in src_files:
    full_file_name = os.path.join(path + '/OrthogonalGraphs/Missing_Edges/', file_name)
    if (os.path.isfile(full_file_name)):
        copyfile(full_file_name, path + '/OrthogonalGraphs/Repaired/'+file_name)

complete_nodes_shapefile = ogr.Open(path+'/OrthogonalGraphs/Complete/nodes.shp', 1)
complete_nodes_layer = complete_nodes_shapefile.GetLayerByIndex(0)
complete_edges_shapefile = ogr.Open(path+'/OrthogonalGraphs/Complete/edges.shp', 1)
complete_edges_layer = complete_edges_shapefile.GetLayerByIndex(0)
incomplete_nodes_shapefile = ogr.Open(path+'/OrthogonalGraphs/Repaired/nodes.shp', 1)
incomplete_nodes_layer = incomplete_nodes_shapefile.GetLayerByIndex(0)
incomplete_edges_shapefile = ogr.Open(path+'/OrthogonalGraphs/Repaired/edges.shp', 1)
incomplete_edges_layer = incomplete_edges_shapefile.GetLayerByIndex(0)

to_be_found = [] #contains edges that have been removed and will have to be found by the reconstructor
for complete_edge in complete_edges_layer:
    incomplete_edges_layer.ResetReading()
    temp = []
    found = False
    temp.append(complete_edge.GetFieldAsDouble(1))
    temp.append(complete_edge.GetFieldAsDouble(2))
    temp.append(complete_edge.GetFieldAsDouble(3))
    temp.append(complete_edge.GetFieldAsDouble(4))
    for incomplete_edge in incomplete_edges_layer:
        temp1 = []
        temp1.append(incomplete_edge.GetFieldAsDouble(1))
        temp1.append(incomplete_edge.GetFieldAsDouble(2))
        temp1.append(incomplete_edge.GetFieldAsDouble(3))
        temp1.append(incomplete_edge.GetFieldAsDouble(4))
        if (temp[:2] == temp1[:2] and temp[2:]==temp1[2:]) or (temp[:2] == temp1[2:] and temp[2:]==temp1[:2]):
            found = True
            pass
    if not found:
        to_be_found.append(temp)


new_edges = repair(complete_nodes_layer, complete_edges_layer, incomplete_nodes_layer, incomplete_edges_layer)


repaired_edges_shapefile = ogr.Open(path+'/OrthogonalGraphs/Repaired/edges.shp', 1)
repaired_edges_layer = repaired_edges_shapefile.GetLayerByIndex(0)

calculate_similarity(new_edges, to_be_found)


print("ok")
