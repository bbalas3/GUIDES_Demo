import shapefile
import osgeo.ogr, osgeo.osr
from osgeo import ogr
import sys
import os
import time
import random
import networkx as nx
from networkx.classes.graph import Graph
from shutil import copyfile
import csv
from shapely.geometry import shape
import queue
from queue import Queue

path = os.getcwd()
path_infrastructure = path+'/Infrastructure/'
path_pipes = path+'/Infrastructure/PipesConverted/'

def clean_duplicate_nodes():
    nodes_shapefile = ogr.Open(path_pipes+"Clean/clean_Nodes_new-C-WATR-UNDR.shp",1 )
    edges_shapefile = ogr.Open(path_pipes+"Clean/clean_Edges_new-C-WATR-UNDR.shp",1 )
    nodes_layer = nodes_shapefile.GetLayerByIndex(0)
    edges_layer = edges_shapefile.GetLayerByIndex(0)
    g = nx.read_shp(path_pipes, True)
    g = g.to_undirected(False)
    maxID = edges_layer.GetFeature(edges_layer.GetFeatureCount()-1).GetFieldAsInteger(0) +1

    #Now let's create two lists so that we don't have to access the
    #layer all the time. This is done for efficiency reasons
    coor_x_list = []
    coor_y_list = []
    id_list = [] #this is useful because there are gaps in the ids of the features (e.g. 59 is missing)
    for feature in range(nodes_layer.GetFeatureCount()):
        id_list.append(nodes_layer.GetFeature(feature).GetFieldAsInteger(0))
        coor_x_list.append(nodes_layer.GetFeature(feature).GetFieldAsDouble(1))
        coor_y_list.append(nodes_layer.GetFeature(feature).GetFieldAsDouble(2))

    #Now we want to track all the vertices that are so close to be practically the same node
    #We store their featureID in the list nodes_to_merge
    nodes_to_merge = []
    loop_length = len(coor_x_list)
    for feature1 in range(loop_length):
        if feature1%1000 == 0: #just to see how long it takes to do 1000 features
            print (feature1)
        coor_x1 = coor_x_list[feature1]
        coor_y1 = coor_y_list[feature1]
        for feature2 in range(loop_length):
            coor_x2 = coor_x_list[feature2]
            coor_y2 = coor_y_list[feature2]
            if feature1 != feature2 and abs(coor_x1 - coor_x2) <= 0.01 and abs(coor_y1 - coor_y2) <= 0.01:
                if not(([feature1, feature2] in nodes_to_merge) or ([feature2, feature1] in nodes_to_merge)):
                    nodes_to_merge.append([feature1, feature2])

    #Now we:
    #1) delete the one of the two duplicate nodes (the second in the pairs in nodes_to_merge)
    #2) connect to the first node the edges they were attached to the second node
    already_deleted = []
    with open(path_pipes+'Clean/clean_Edgelist_new-C-WATR-UNDR.csv', 'r') as csvfile:
        for nodes in nodes_to_merge:
            reader = csv.reader(csvfile)
            next(reader, None) #skips the header of the csv file
            for line in reader:
                edge = ogr.Geometry(ogr.wkbLineString)
                featureDefn = edges_layer.GetLayerDefn()
                edge_feature = ogr.Feature(featureDefn)
                #print line
                start_node_is_same = abs(coor_x_list[nodes[1]]-float(line[0])) <= 0.0001 and abs(coor_y_list[nodes[1]]-float(line[1])) <= 0.0001
                end_node_is_same = abs(coor_x_list[nodes[1]]-float(line[2])) <= 0.0001 and abs(coor_y_list[nodes[1]]-float(line[3])) <= 0.0001
                if start_node_is_same:
                    #add edge
                    edge.AddPoint(float(line[2]), float(line[3])) #end node of the edge
                    edge.AddPoint(coor_x_list[nodes[0]], coor_y_list[nodes[0]])
                    edge_feature.SetGeometry(edge)
                    edge_feature.SetField("Edge", maxID)
                    edge_feature.SetField("x_start", float(line[2]))
                    edge_feature.SetField("y_start", float(line[3]))
                    edge_feature.SetField("x_end", coor_x_list[nodes[0]])
                    edge_feature.SetField("y_end", coor_y_list[nodes[0]])
                    maxID += 1
                    edges_layer.CreateFeature(edge_feature)
                    #remove edge
                    edges_shapefile.GetLayerByIndex(0).DeleteFeature(int(float(line[6]))) #line[6] contains the edge's ID
                if end_node_is_same:
                    #add edge
                    edge.AddPoint(float(line[0]), float(line[1])) #end node of the edge
                    edge.AddPoint(coor_x_list[nodes[0]], coor_y_list[nodes[0]])
                    edge_feature.SetGeometry(edge)
                    edge_feature.SetField("Edge", maxID)
                    edge_feature.SetField("x_start", float(line[0]))
                    edge_feature.SetField("y_start", float(line[1]))
                    edge_feature.SetField("x_end", coor_x_list[nodes[0]])
                    edge_feature.SetField("y_end", coor_y_list[nodes[0]])
                    maxID += 1
                    edges_layer.CreateFeature(edge_feature)
                    #remove edge
                    edges_shapefile.GetLayerByIndex(0).DeleteFeature(int(float(line[6]))) #line[6] contains the edge's ID
            if nodes[1] not in already_deleted:
                nodes_shapefile.GetLayerByIndex(0).DeleteFeature(nodes[1])
                already_deleted.append(nodes[1])
                nodes_layer.SyncToDisk()
    nodes_shapefile.SyncToDisk()
    edges_shapefile.SyncToDisk()


def replace_loops():
    print ("replacing loops...")
    nodes_shapefile = ogr.Open(path_pipes+"NoLoops/noloops_Nodes_new-C-WATR-UNDR.shp",1 )
    edges_shapefile = ogr.Open(path_pipes+"NoLoops/noloops_Edges_new-C-WATR-UNDR.shp",1 )
    nodes_layer = nodes_shapefile.GetLayerByIndex(0)
    edges_layer = edges_shapefile.GetLayerByIndex(0)
    g = nx.read_shp(path_pipes+'NoLoops/', True)
    g = g.to_undirected(False)
    tempG = g #we need it to correctly iterate in the for loop that follows
    connected_Components = list(nx.connected_components(tempG))


    #We create a list containing all the nodes with degree 1
    #(i.e. all the nodes with only one edge), for efficiency purposes
    nodes_with_degree_1 = []
    for n in g.nodes():
        if nx.degree(g, n) == 1:
            nodes_with_degree_1.append(n)

    nodes_loop = [] #contains the nodes that have the loop edge

    nodefeatureDefn = nodes_layer.GetLayerDefn()
    node_feature = ogr.Feature(nodefeatureDefn)
    drain_field = ogr.FieldDefn()
    drain_field.SetName("Is_Drain")
    drain_field.SetType(ogr.OFTInteger)
    nodes_layer.CreateField(drain_field)

    edgefeatureDefn = edges_layer.GetLayerDefn()
    edge_feature = ogr.Feature(edgefeatureDefn)
    maxID_node = nodes_layer.GetFeature(nodes_layer.GetFeatureCount()-1).GetFieldAsInteger(0) +1
    maxID_edge = edges_layer.GetFeature(edges_layer.GetFeatureCount()-1).GetFieldAsInteger(0) +1

    edge_list = [] #useful for efficiency reasons
    for feature in range(maxID_edge):
        temp = []
        if edges_layer.GetFeature(feature) is not None:
            temp.append(edges_layer.GetFeature(feature).GetFieldAsInteger(0))
            temp.append(edges_layer.GetFeature(feature).GetFieldAsString(1))
            temp.append(edges_layer.GetFeature(feature).GetFieldAsInteger(2))
            temp.append(edges_layer.GetFeature(feature).GetFieldAsDouble(3))
            temp.append(edges_layer.GetFeature(feature).GetFieldAsDouble(4))
            temp.append(edges_layer.GetFeature(feature).GetFieldAsDouble(5))
            temp.append(edges_layer.GetFeature(feature).GetFieldAsDouble(6))
            temp.append(edges_layer.GetFeature(feature).GetFieldAsDouble(7))
            edge_list.append(temp)
    already_deleted_edges = []

    nodes_list = [] #for efficiency reasons
    for feature in range(nodes_layer.GetFeatureCount()):
        temp = []
        if nodes_layer.GetFeature(feature) is not None:
            temp.append(nodes_layer.GetFeature(feature).GetFieldAsInteger(0))
            temp.append(nodes_layer.GetFeature(feature).GetFieldAsDouble(1))
            temp.append(nodes_layer.GetFeature(feature).GetFieldAsDouble(2))
            nodes_list.append(temp)

    for c in connected_Components:
        subG = g.subgraph(c)
        nodes_to_connect = [] #nodes that we want to connect to the current loop
        if subG.number_of_nodes() == 1 and subG.number_of_edges() == 1:
            for node in nodes_with_degree_1:
                if abs(node[0] - subG.nodes()[0][0]) <= 1 and abs(node[1] - subG.nodes()[0][1]) <= 1:
                    nodes_to_connect.append(node)
            if len(nodes_to_connect) >= 2:
                new_coor_x = (nodes_to_connect[0][0] + nodes_to_connect[1][0])/2
                new_coor_y = (nodes_to_connect[0][1] + nodes_to_connect[1][1])/2
                found = False
                id_node_to_delete = None
                for feature in range(nodes_layer.GetFeatureCount()):
                    current_node = nodes_layer.GetFeature(feature)
                    if current_node is not None and abs(current_node.GetFieldAsDouble(1) - subG.nodes()[0][0]) <= 0.0001 and abs(current_node.GetFieldAsDouble(2) - subG.nodes()[0][1]) <= 0.0001:
                        id_node_to_delete = feature
                        pass #continue
                #add the new node
                point = ogr.Geometry(ogr.wkbPoint)
                point.AddPoint(new_coor_x, new_coor_y)
                node_feature.SetGeometry(point)
                node_feature.SetField("Is_Drain", 1)
                node_feature.SetField("Node", maxID_node)
                node_feature.SetField("coord_x", new_coor_x)
                node_feature.SetField("coord_y", new_coor_y)
                nodes_layer.CreateFeature(node_feature)
                maxID_node += 1

                #add the first edge
                edge1 = ogr.Geometry(ogr.wkbLineString)
                edge1.AddPoint(nodes_to_connect[0][0], nodes_to_connect[0][1])
                edge1.AddPoint(new_coor_x, new_coor_y)
                edge_feature.SetField("x_end", new_coor_x)
                edge_feature.SetField("y_end", new_coor_y)
                edge_feature.SetField("x_start", nodes_to_connect[0][0])
                edge_feature.SetField("y_start", nodes_to_connect[0][1])
                edge_feature.SetGeometry(edge1)
                edge_feature.SetField("Edge", maxID_edge)
                edges_layer.CreateFeature(edge_feature)
                maxID_edge += 1

                #add the second edge
                edge2 = ogr.Geometry(ogr.wkbLineString)
                edge2.AddPoint(nodes_to_connect[1][0], nodes_to_connect[1][1])
                edge2.AddPoint(new_coor_x, new_coor_y)
                edge_feature.SetField("x_start", nodes_to_connect[1][0])
                edge_feature.SetField("y_start", nodes_to_connect[1][1])
                edge_feature.SetField("x_end", new_coor_x)
                edge_feature.SetField("y_end", new_coor_y)
                edge_feature.SetGeometry(edge2)
                edge_feature.SetField("Edge", maxID_edge)
                edges_layer.CreateFeature(edge_feature)
                maxID_edge +=1

                #delete the node in the loop
                if id_node_to_delete is not None:
                    nodes_layer.DeleteFeature(id_node_to_delete)

                #delete the edge of the loop
                for i in range(len(edge_list)):
                    if i not in already_deleted_edges:
                        x_start = edge_list[i][4]
                        y_start = edge_list[i][5]
                        x_end = edge_list[i][6]
                        y_end = edge_list[i][7]
                        if x_start == x_end and y_start == y_end and abs(x_start - subG.nodes()[0][0]) <= 0.0001:
                            edges_layer.DeleteFeature(edge_list[i][0]-1)
                            already_deleted_edges.append(i)

                    edges_shapefile.SyncToDisk()
                    nodes_shapefile.SyncToDisk()
    edges_shapefile.SyncToDisk()
    nodes_shapefile.SyncToDisk()
            ###MAKE IF STATEMENT ON LINE 153 MORE EFFICIENT BY USING LISTS (LIKE IN THE OTHER METHOD)
#                       nodes_to_replace.append(list(node_to_flag))
#               for edge_to_flag in subG.edges():
#                   edges_to_flag.append(list(edge_to_flag))
#

def replace_T_shapes():
    #to find the T shapes, we want to find a node that is surrounded by 9 nodes within a certain range
    #defined by delta_x and delta_y. To find the surrounding nodes, we do not loop over all the existing nodes,
    #but only on the 9 with the closest IDs (because they will be in similar areas on the map. This improves efficiency
    g = nx.read_shp(path_infrastructure+'/PipesConverted/NoJoints', True)
    g = g.to_undirected(False)
    nodes_shapefile = ogr.Open(path_pipes+"NoJoints/nojoints_Nodes_new-C-WATR-UNDR.shp", 1)
    edges_shapefile = ogr.Open(path_pipes+"NoJoints/nojoints_Edges_new-C-WATR-UNDR.shp", 1)
    edges_layer = edges_shapefile.GetLayerByIndex(0)
    nodes_layer = nodes_shapefile.GetLayerByIndex(0)
    current_nodes = [] #contains the 20 nodes currently being examined
    
    nodes_to_remove = [] #keeps track of the nodes that will have to be removed
    edges_to_remove = [] #keeps track of the edges that will have to be removed
    #edges_to_edit = [] #keeps track of the edges that will have to be edited (they will be connected to the central node)
    edges_to_add = []
    central_nodes = [] #contain all the nodes that are at the center of a T shape
    features = []
    feature = nodes_layer.GetNextFeature()
    temp = []
    delta_x1 = 0.55
    delta_y1 = 0.7
    delta_x2 = 0.4
    delta_y2 = 0.5
    while feature:
        temp = []
        temp.append(feature.GetFieldAsInteger(0))
        temp.append(feature.GetFieldAsDouble(1))
        temp.append(feature.GetFieldAsDouble(2))
        features.append(temp)
        feature = nodes_layer.GetNextFeature()
    print("here0")
    for central_node in features:
        if central_node[0]%1000==0:
            print(central_node[0])
        found = 0
        temp = []
        surrounding_nodes = []
        for surrounding_node in features:
            if surrounding_node[0] == central_node[0]: #don't want to check a node with itself
                continue
            not_too_far = abs(central_node[1]-surrounding_node[1]) < delta_x1 and abs(central_node[2]-surrounding_node[2]) < delta_y1
            not_too_close = abs(central_node[1]-surrounding_node[1]) > delta_x2 or abs(central_node[2]-surrounding_node[2]) > delta_y2
            if not_too_far and not_too_close:
                found += 1
                surrounding_nodes.append(surrounding_node)
                temp.append(surrounding_node[0])
                if found == 9:
                    break
        if found == 9:
            central_nodes.append(central_node[0])
            nodes_to_remove += temp
            for node in surrounding_nodes:
                if (nx.degree(g, (node[1], node[2]))) == 1: #is one of the perpendicular edges
                    temp_edge = nx.edges(g, (node[1], node[2]))
                    #print(temp_edge)
                    edges_to_remove.append([[temp_edge[0][0][0], temp_edge[0][0][1]], [temp_edge[0][1][0], temp_edge[0][1][1]]])
                else:
                    edges = nx.edges(g, (node[1], node[2]))
                    for edge in edges:
                        if (central_node[1], central_node[2]) in edge: #connects the surrounding node to the central node
                            edges_to_remove.append([[edge[0][0], edge[0][1]], [edge[1][0], edge[1][1]]])
                        else: #it's an edge that connects the T shape to the rest of the network
                            edges_to_add.append([[edge[1][0], edge[1][1]], [central_node[1], central_node[2]]])
                            
    #print(edges_to_remove)         
    #print(nodes_to_remove)
    #now that we have established wich nodes/edges need to be added/removed, we edit the shapefiles
    nodes_shapefile = ogr.Open(path_pipes+"NoJoints/nojoints_Nodes_new-C-WATR-UNDR.shp", 1) #necessary to reuse .getNextFeature()
    nodes_layer = nodes_shapefile.GetLayerByIndex(0) 
    feature = nodes_layer.GetNextFeature()
    print("here")
    nodefeatureDefn = nodes_layer.GetLayerDefn()
    node_feature = ogr.Feature(nodefeatureDefn)
    joint_field = ogr.FieldDefn()
    joint_field.SetName("Joint")
    joint_field.SetType(ogr.OFTInteger)
    nodes_layer.CreateField(joint_field)
    nodes_layer.SyncToDisk()
    
    #delete the nodes and mark the central nodes as joints
    while feature: #take a node
        featureID = feature.GetFID()
        #print(featureID)
        #print(feature.GetFieldAsInteger(0))
        if feature.GetFieldAsInteger(0) in nodes_to_remove: #if it is
            nodes_layer.DeleteFeature(featureID) #delete it
        if feature.GetFieldAsInteger(0) in central_nodes:
            feature.SetField("Joint", 1)
            nodes_layer.SetFeature(feature)
            #print("del")
            #nodes_layer.SyncToDisk()
        feature = nodes_layer.GetNextFeature()
    nodes_shapefile.SyncToDisk()
    
    #delete the edges
    feature = edges_layer.GetNextFeature()
    while feature:
        featureID = feature.GetFID()
        if featureID%1000==0:
            print(featureID)
        for edge in edges_to_remove:
            x_start = feature.GetFieldAsDouble(4)
            y_start = feature.GetFieldAsDouble(5)
            x_end = feature.GetFieldAsDouble(6)
            y_end = feature.GetFieldAsDouble(7)
            #we create four boolean variables to check if the current feature(edge) is the same as the current edge(in the list edges_to_remove)
            #because an edge (a,b) can also be written as (b,a), we need to keep this in mind when
            #looking for the edge, hence the reason why create the following variables
            first_equals_first = abs(x_start - edge[0][0]) <= 0.0001 and abs(y_start - edge[0][1]) <= 0.0001
            second_equals_second = abs(x_end - edge[1][0]) <= 0.0001 and abs(y_end - edge[1][1]) <= 0.0001
            first_equals_second = abs(x_start - edge[1][0]) <= 0.0001 and abs(y_start - edge[1][1]) <= 0.0001
            second_equals_first = abs(x_end - edge[0][0]) <= 0.0001 and abs(y_end - edge[0][1]) <= 0.0001
            if (first_equals_first and second_equals_second) or (first_equals_second and second_equals_first):
                edges_layer.DeleteFeature(featureID)
                edges_to_remove.remove(edge)
                break
        feature = edges_layer.GetNextFeature()
    edges_shapefile.SyncToDisk()
    
    #print(edges_to_add)
    print(edges_to_add.__len__())
    
    #add the new edges
    maxID = edges_layer.GetFeature(edges_layer.GetFeatureCount()-1).GetFieldAsInteger(0) + 1
    for new_edge in edges_to_add:
        edge = ogr.Geometry(ogr.wkbLineString)
        featureDefn = edges_layer.GetLayerDefn()
        edge_feature = ogr.Feature(featureDefn)
        edge.AddPoint(new_edge[0][0], new_edge[0][1])
        edge.AddPoint(new_edge[1][0], new_edge[1][1])
        edge_feature.SetGeometry(edge)
        edge_feature.SetField("Edge", maxID)
        edge_feature.SetField("x_start", new_edge[0][0])
        edge_feature.SetField("y_start", new_edge[0][1])
        edge_feature.SetField("x_end", new_edge[1][0])
        edge_feature.SetField("y_end", new_edge[1][1])
        maxID += 1
        edges_layer.CreateFeature(edge_feature)
    edges_shapefile.SyncToDisk()    
        
    print("T-shapes removed")  
    
    

def flag():
    ##this method creates a multipolygon representing the shapes of the buildings
    ##and then checks if there are water pipes with degree = 1 that do not fall within these shapes.
    ##If there are, these nodes must be flagged as mistakes
    buildings_graph = nx.read_shp(path_infrastructure+'/BuildingsConverted/', True)
    buildings_graph = buildings_graph.to_undirected(False)

    poly_list = []
    connected_Components = list(nx.connected_components(buildings_graph))
    multipoly = ogr.Geometry(ogr.wkbMultiPolygon)
    for c in connected_Components:
        subG = buildings_graph.subgraph(c)
        for node in range(len(subG.nodes())):
            try:
                cycle = nx.find_cycle(buildings_graph, subG.nodes()[node]) #raises an exception if there is no cycle
            except:
                pass
            if len(cycle) >= 1:
                ring = ogr.Geometry(ogr.wkbLinearRing) #this will be the polygon
                for edge in cycle:
                    ring.AddPoint(edge[0][0], edge[0][1]) #add the coordinates of the points to the ring
                ring.AddPoint(cycle[0][0][0], cycle[0][0][1]) #close the ring by adding the first (and last) node manually
                poly = ogr.Geometry(ogr.wkbPolygon)
                poly.AddGeometry(ring)
                multipoly.AddGeometry(poly) #I may end up not using this variable for containment tests because it is very not efficient
                poly_list.append(poly) #I will probably use this variable instead, and loop through its elements
                ring = None
                poly = None
    #################
    ##UNCOMMENT TO VISUALIZE THE AREAS THAT HAVE BEEN POLYGONIZED
#     driver = ogr.GetDriverByName('Esri Shapefile')
#     ds = driver.CreateDataSource('/home/mangholb/test.shp')
#     layer = ds.CreateLayer('', None, ogr.wkbMultiPolygon)
#     layer.CreateField(ogr.FieldDefn('id', ogr.OFTInteger))
#     defn = layer.GetLayerDefn()
#     feat = ogr.Feature(defn)
#     feat.SetField('id', 123)
#     geom = ogr.CreateGeometryFromWkb(multipoly.ExportToWkb())
#     feat.SetGeometry(geom)
#     layer.CreateFeature(feat)
#     
#     layer.SyncToDisk()
    #################
    pipes_graph = nx.read_shp(path_pipes+'/Flagged/', True)
    pipes_graph = pipes_graph.to_undirected(False)
    nodes_with_degree_1 = []
    for n in pipes_graph.nodes(): #find all the nodes in the pipes graph that have degree 1
        if nx.degree(pipes_graph, n) == 1:
            nodes_with_degree_1.append(n)


    nodes_shapefile = ogr.Open(path_pipes+"Flagged/flagged_Nodes_new-C-WATR-UNDR.shp", 1)
    nodes_layer = nodes_shapefile.GetLayerByIndex(0)

    nodefeatureDefn = nodes_layer.GetLayerDefn()
    node_feature = ogr.Feature(nodefeatureDefn)
    flagged_field = ogr.FieldDefn()
    flagged_field.SetName("Flagged")
    flagged_field.SetType(ogr.OFTInteger)
    nodes_layer.CreateField(flagged_field)
    nodes_layer.SyncToDisk()

    coor_x_list = [] #for efficiency reasons (we don't want to access the shapefile in the memory all the time)
    coor_y_list = [] #for efficiency reasons (we don't want to access the shapefile in the memory all the time)
    id_list = [] #contains what THE LIBRARY sees as ID, NOT THE ID IN THE ATTRIBUTE TABLE
    maxID_node = nodes_layer.GetFeature(nodes_layer.GetFeatureCount()-1).GetFieldAsInteger(0) +1
    for feature in range(maxID_node):
        if nodes_layer.GetFeature(feature) is not None:
            id_list.append(feature)
            coor_x_list.append(nodes_layer.GetFeature(feature).GetFieldAsDouble(1))
            coor_y_list.append(nodes_layer.GetFeature(feature).GetFieldAsDouble(2))
    nodes_to_flag = []
    i = 0
    for n in nodes_with_degree_1:
        i = i+1
        if i%1000==0:
            print (i) #just so we know how long this method is taking
        point = ogr.Geometry(ogr.wkbPoint)
        point.AddPoint(n[0], n[1])
        is_in_building = False
        #Instead of checking if a point is inside the multipolygon, we check "manually"
        #for the presence of the point in any of the single polygons. This is tens of times
        #more efficient than just doing point.Within(multipoly). Which is the reason for the following for loop
        for poly in poly_list: #check if the point is in any of the buildings
            if point.Within(poly):
                is_in_building = True
                pass #continue
        if not is_in_building: #point.Within(multipoly):
            for index in range(len(coor_x_list)):
                if abs(coor_x_list[index]-n[0]) <= 0.0001 and abs(coor_y_list[index]-n[1]) <= 0.0001:
#UNCOMMENT THE FOLLOWING to help debug the issues with the IDs
#                                       print n
#                                       print coor_x_list[index]
#                                       print coor_y_list[index]
#                                       print id_list[index]
#                                       print index
#                                       print nodes_layer.GetFeature(id_list[index]).GetFieldAsDouble(1)
#                                       print nodes_layer.GetFeature(id_list[index]).GetFieldAsDouble(2)
#                                       print "//////////////////////////"
                    to_be_set = nodes_layer.GetFeature(id_list[index])
                    to_be_set.SetField("Flagged", 1)
                    nodes_layer.SetFeature(to_be_set)
                    nodes_layer.SyncToDisk()
        point = None
    nodes_layer.SyncToDisk()
