import osgeo.ogr
from osgeo import ogr
import sys
import os
import time
import random
import networkx as nx
from networkx.classes.graph import Graph
from shutil import copyfile


def remove_single_nodes(g, incomplete_nodes_layer):
    # now we want to remove all the isolated nodes
    to_be_removed = []
    connected_Components = list(nx.connected_components(g))
    for c in connected_Components:
        subG = g.subgraph(c)
        if subG.number_of_nodes() == 1:  # if a subgraph is just one isolated node
            to_be_removed.append(subG.nodes()[0])  # add it to a list of nodes to be removed

            # remove the nodes in the list
    for node in to_be_removed:
        g.remove_node(node)
        if incomplete_nodes_layer is not None:
            for feature in range(incomplete_nodes_layer.GetFeatureCount()):
                current_feature = incomplete_nodes_layer.GetFeature(feature)
                if current_feature is not None:
                    if node[0] == current_feature.GetFieldAsDouble(1) and node[1] == current_feature.GetFieldAsDouble(2):
                        incomplete_nodes_layer.DeleteFeature(feature)

    return g

def add_nodes_crossings(g):
     # if edge is vertical, look for edges that cross horizontally
     # if edge is horizontal, look for edges that cross vertically
     # First of all we create two lists depending on the direction of the edges,
     # to improve the performance when looking for crossing edges (the crossings
     # we're looking for always involve one vertical and one horiontal edge).
     #Eventually, we need to make sure that the nodes we create aren't floating
     #in the middle of nothing, but actually connect edges correctly (and split
     #the current ones).
    vertical_edges = get_vertical_edges(g)
    horizontal_edges = get_horizontal_edges(g)
    to_be_removed = []
    to_be_added = []
    for evertical in vertical_edges:
        for ehorizontal in horizontal_edges:
            if ((evertical[0][1] <= ehorizontal[0][1] <= evertical[1][1]) or (evertical[1][1] <= ehorizontal[0][1] <= evertical[0][1])) and ((ehorizontal[0][0] <= evertical[0][0] <= ehorizontal[1][0]) or (ehorizontal[1][0] <= evertical[0][0] <= ehorizontal[0][0])):
                g.add_node((evertical[0][0], ehorizontal[0][1]))
                to_be_added.append(((evertical[0][0], ehorizontal[0][1]), (evertical[0][0], evertical[0][1])))
                to_be_added.append(((evertical[0][0], ehorizontal[0][1]), (evertical[1][0], evertical[1][1])))
                to_be_added.append(((evertical[0][0], ehorizontal[0][1]), (ehorizontal[0][0], ehorizontal[0][1])))
                to_be_added.append(((evertical[0][0], ehorizontal[0][1]), (ehorizontal[1][0], ehorizontal[1][1])))
                if not(evertical in to_be_removed):
                    to_be_removed.append(evertical)
                if not(ehorizontal in to_be_removed):
                    to_be_removed.append(ehorizontal)

    for e in to_be_removed:
        g.remove_edge(e[0], e[1])
    for e in to_be_added:
        g.add_edge(e[0], e[1])
    return g

def remove_double_edges(g, gcopy):
    to_be_removed = []
    i = 0
    for edge1 in g.edges_iter():
        k = 0
        for edge2 in gcopy.edges_iter():
            first_first_nodes_same = (edge1[0][0] == edge2[0][0] and edge1[0][1] == edge2[0][1])
            second_second_nodes_same = (edge1[1][0] == edge2[1][0] and edge1[1][1] == edge2[1][1])
            first_second_nodes_same = (edge1[0][0] == edge2[1][0] and edge1[0][1] == edge2[1][1])
            second_first_nodes_same = (edge1[1][0] == edge2[0][0] and edge1[1][1] == edge2[0][1])
            already_added = [[edge2[0][0], edge2[0][1]], [edge2[1][0], edge2[1][1]]] in to_be_removed
            already_added = already_added or [[edge2[1][0], edge2[1][1]], [edge2[0][0], edge2[0][1]]] in to_be_removed
            if i < k and ((first_first_nodes_same and second_second_nodes_same) or (first_second_nodes_same and second_first_nodes_same)) and (not already_added):
                to_be_removed.append([[edge1[0][0], edge1[0][1]], [edge1[1][0], edge1[1][1]]])
            k += 1
        i += 1
    for e in to_be_removed:
        g.remove_edge((e[0][0],e[0][1]), (e[1][0],e[1][1]))
    return g

def get_vertical_edges(g):
    vertical_edges = []
    for e in g.edges_iter():
        if e[0][0] == e[1][0]:  # this is a vertical edge
            vertical_edges.append(e)
    return vertical_edges

def get_horizontal_edges(g):
    horizontal_edges = []
    for e in g.edges_iter():
        if e[0][1] == e[1][1]:  # this is a horizontal edge
            horizontal_edges.append(e)
    return horizontal_edges

def clean_overlapping_edges(g):
    #split an edge in many parts depending on how many nodes happen to be on it
    vertical_edges = get_vertical_edges(g)
    horizontal_edges = get_horizontal_edges(g)
    to_be_removed =[]
    to_be_added = []
    nodes_to_process = {}

    for e in vertical_edges: #We start working on the vertical edges
        nodes_to_process = {}
        nodes_to_process[e[0][1]] = e[0] # .add(e[0][1]=e[0])
        for n in g.nodes():
            if n==e[0] or n==e[1]:
                continue
            if n[0]==e[0][0] and ((e[0][1] <= n[1] <= e[1][1]) or (e[1][1] <= n[1] <= e[0][1])):
                nodes_to_process[n[1]] = n # .add .append(n)
        nodes_to_process[e[1][1]] = e[1] #.append(e[1])
        nodes_to_process = sorted(nodes_to_process.items())
        if(len(nodes_to_process) > 2):
            to_be_added = to_be_added + create_adjacent_edges(nodes_to_process)
            to_be_removed.append(e)

    for e in horizontal_edges: #now we work on horizontal edges
        nodes_to_process = {}
        nodes_to_process[e[0][0]] = e[0] # .add(e[0][1]=e[0])
        for n in g.nodes():
            if n==e[0] or n==e[1]:
                continue
            if n[1]==e[0][1] and ((e[0][0] <= n[0] <= e[1][0]) or (e[1][0] <= n[0] <= e[0][0])):
                nodes_to_process[n[0]] = n # .add .append(n)
        nodes_to_process[e[1][0]] = e[1] #.append(e[1])
        nodes_to_process = sorted(nodes_to_process.items())
        if(len(nodes_to_process) > 2):
            to_be_added = to_be_added + create_adjacent_edges(nodes_to_process)
            to_be_removed.append(e)
    for e in to_be_removed:
        g.remove_edge(e[0], e[1])
    for e in to_be_added:
        g.add_edge(e[0], e[1])
    return g

def create_adjacent_edges(nodes_to_process): #creates adjacent edges that connect a list of nodes
    edges = []
    for i in range(0, len(nodes_to_process)-1):
        edges.append((nodes_to_process[i][1], nodes_to_process[i+1][1]))
    return edges

def split_edges(g, edges_to_split): #creates gaps (artificial errors) in the edges from the list nodes_to_split
    to_be_added = []
    for e in edges_to_split:
        if e[0][0] == e[1][0]: #it's a vertical edge
            greater = max(e[0][1], e[1][1])
            lesser = min(e[0][1], e[1][1])
            edge_length = greater - lesser #this gives how long the edge is
            gap_length = int(edge_length/3) #the gaps we create will be 1/3 of the total length of the edge
            #The gap can be in either one of the following positions:
            #1. It is in the middle of the edge
            #2. It is adjacent to the first node (the one with greater ycoord)
            #3. It is adjacent to the second node (the one with lesser ycoord)
            #We handle the three situations randomly
            choice = random.randint(1,3)
            if choice == 1:
                to_be_added.append(((e[0][0], lesser), (e[0][0], lesser+gap_length)))
                to_be_added.append(((e[0][0], greater), (e[0][0], greater-gap_length)))
            if choice == 2:
                to_be_added.append(((e[0][0], greater), (e[0][0], lesser+gap_length)))
            if choice == 3:
                to_be_added.append(((e[0][0], lesser), (e[0][0], greater-gap_length)))
        if e[0][1] == e[1][1]: #it's a horizontal edge
            greater = max(e[0][0], e[1][0])
            lesser = min(e[0][0], e[1][0])
            edge_length = greater - lesser #this gives how long the edge is
            gap_length = int(edge_length/3) #the gaps we create will be 1/3 of the total length of the edge
            #the three cases that follow are symmetric to the ones for vertical edges
            choice = random.randint(1,3)
            if choice == 1:
                to_be_added.append(((lesser, e[0][1]), (lesser+gap_length, e[0][1])))
                to_be_added.append(((greater, e[0][1]), (greater-gap_length, e[0][1])))
            if choice == 2:
                to_be_added.append(((greater, e[0][1]), (lesser+gap_length, e[0][1])))
            if choice == 3:
                to_be_added.append(((lesser, e[0][1]), (greater-gap_length, e[0][1])))
    for e in edges_to_split:
        g.remove_edge(e[0], e[1])
    for e in to_be_added:
        g.add_edge(e[0], e[1])
    return g

def network_to_shp(g, path_to_write):
    #delete all the content in the Complete folder (otherwise you can't run the main twice in a row)
    for the_file in os.listdir(path_to_write):
        file_path = os.path.join(path_to_write, the_file)
        if os.path.isfile(file_path):
            os.unlink(file_path)

    #We want to save the graph to a shapefile, while also adding the proper fields to the attribute table
    #SHAPEFILE CONTAINING THE NODES
    driver = ogr.GetDriverByName('Esri Shapefile')
    ds = driver.CreateDataSource(path_to_write +'nodes.shp')
    nodes_layer = ds.CreateLayer('', None, ogr.wkbPoint)
    nodes_layer.CreateField(ogr.FieldDefn('id', ogr.OFTInteger))
    nodes_layer.CreateField(ogr.FieldDefn('coord_x', ogr.OFTReal))
    nodes_layer.CreateField(ogr.FieldDefn('coord_y', ogr.OFTReal))
    nodes_layer.CreateField(ogr.FieldDefn('flagged', ogr.OFTInteger))
    #add the nodes, one by one
    nodefeatureDefn = nodes_layer.GetLayerDefn()
    i = 1
    for node in g.nodes():
        nodefeat = ogr.Feature(nodefeatureDefn)
        nodefeat.SetField('id', i)
        i = i+1
        nodefeat.SetField('coord_x', node[0])
        nodefeat.SetField('coord_y', node[1])
        point = ogr.Geometry(ogr.wkbPoint)
        point.AddPoint(node[0], node[1])
        nodefeat.SetGeometry(point)
        nodes_layer.CreateFeature(nodefeat)
    nodes_layer.SyncToDisk()

    #SHAPEFILE CONTAINING THE EDGES
    ds = driver.CreateDataSource(path_to_write + 'edges.shp')
    edges_layer = ds.CreateLayer('', None, ogr.wkbLineString)
    edges_layer.CreateField(ogr.FieldDefn('id', ogr.OFTInteger))
    edges_layer.CreateField(ogr.FieldDefn('x_start', ogr.OFTReal))
    edges_layer.CreateField(ogr.FieldDefn('y_start', ogr.OFTReal))
    edges_layer.CreateField(ogr.FieldDefn('x_end', ogr.OFTReal))
    edges_layer.CreateField(ogr.FieldDefn('y_end', ogr.OFTReal))
    edges_layer.CreateField(ogr.FieldDefn('diameter', ogr.OFTInteger))
    #add the edges, one by one
    edgefeatureDefn = edges_layer.GetLayerDefn()
    i = 1
    for edge in g.edges():
        edgefeat = ogr.Feature(edgefeatureDefn)
        edgefeat.SetField('id', i)
        i = i+1
        edgefeat.SetField('x_start', edge[0][0])
        edgefeat.SetField('y_start', edge[0][1])
        edgefeat.SetField('x_end', edge[1][0])
        edgefeat.SetField('y_end', edge[1][1])
        line = ogr.Geometry(ogr.wkbLineString)
        line.AddPoint(edge[0][0], edge[0][1])
        line.AddPoint(edge[1][0], edge[1][1])
        edgefeat.SetGeometry(line)
        edges_layer.CreateFeature(edgefeat)
    edges_layer.SyncToDisk()

#with the following method we implement the logic that allows to infer the diameter
#of a pipe given the pipes it is connected to. When a pipe splits, the two pipes it splits into must
#be smaller than the initial pipe. Because we do not have actual values for the diameters, we are just going to use
#an index i that allows for ordering the pipes (e.g. we will be able to say which pipes are bigger than which pipes,
#but not *how big*).
def set_diameters(g, g_shp):
	g = g.to_undirected()
	n_degree_1 = []
	for n in g.nodes():
		if nx.degree(g, n) == 1:
			n_degree_1.append(n)
	nodes = []
	for n in n_degree_1:
		#let's say that half of the nodes with degree==1 are supposed to be end nodes inside of some building
		#therefore they are the starting points to start spreading the indexes from
		if random.randint(1, 2) == 1:
			nodes.append(n)
	
	already_visited = []
	edges_diameters = []
	edges = []
	while nodes: #means: while nodes has elements
		for n in nodes:
			edges.extend(nx.edges(g, n))
		nodes = []
		#print(nodes)
		#print(edges)
		edges_for = edges.copy() #need a different variable for the for loop
		temp = []
		while edges: #means: while edges has elements
			if edges[0] not in already_visited and (edges[0][1], edges[0][0]) not in already_visited:
				already_visited.append(edges[0])
				if nx.degree(g, edges[0][1]) == 2:
					edges.extend(nx.edges(g, edges[0][1]))
				else:
					nodes.append(edges[0][1])
				temp.append(edges[0])
			edges.remove(edges[0])
		if temp: #if temp contains elements
			edges_diameters.append(temp)
	#print(edges_diameters)
	
	edges_layer = g_shp.GetLayerByIndex(0)
	feature = edges_layer.GetNextFeature()
	while feature:
		x_start = feature.GetFieldAsDouble(1)
		y_start = feature.GetFieldAsDouble(2)
		x_end = feature.GetFieldAsDouble(3)
		y_end = feature.GetFieldAsDouble(4)
		edge = ((x_start, y_start), (x_end, y_end))
		edge_reversed = ((x_end, y_end), (x_start, y_start))
		for i in range(len(edges_diameters)):
			if edge in edges_diameters[i] or edge_reversed in edges_diameters[i]:
				feature.SetField("diameter", i)
				edges_layer.SetFeature(feature)
				edges_layer.SyncToDisk()
				pass
		feature = edges_layer.GetNextFeature()
	


def remove_meaningless_nodes(g):
    #sometimes there is a node in the middle of an edge
    #we want to remove them
    g = g.to_undirected()
    finished = False
    while(not finished):
        horizontal_edges = get_horizontal_edges(g)
        vertical_edges = get_vertical_edges(g)
        edges_to_remove = []
        nodes_to_remove = []
        edges_to_add = []
        #print(horizontal_edges)
        for node in g.nodes():
            if nx.degree(g, node) != 2:
                continue
            else:
                edges = nx.edges(g, node)
                edge1 = edges[0]
                edge2 = edges[1]
                
                first_is_horizontal = edge1 in horizontal_edges or (edge1[1], edge1[0]) in horizontal_edges
                second_is_horizontal = edge2 in horizontal_edges or (edge2[1], edge2[0]) in horizontal_edges
                both_horizontal = first_is_horizontal and second_is_horizontal
                
                first_is_vertical = edge1 in vertical_edges or (edge1[1], edge1[0]) in vertical_edges
                second_is_vertical = edge2 in vertical_edges or (edge2[1], edge2[0]) in vertical_edges
                both_vertical = first_is_vertical and second_is_vertical
                
                if both_horizontal or both_vertical: #they have the same y
                    #edges_to_remove.append((edge1[0], edge1[1]))
                    g.remove_edges_from(edge1)
                    nodes_to_remove.append(node)
                    g.add_edge((edge1[1][0], edge1[1][1]), (edge2[1][0], edge2[1][1]))
                    #edges_to_add.append(((edge1[1][0], edge1[1][1]), (edge2[1][0], edge2[1][1])))
    #     for edge in edges_to_remove:
    #         g.remove_edge(edge[0], edge[1])
    #     for edge in edges_to_add:
    #         g.add_edge(edge[0], edge[1])
        for node in nodes_to_remove:
            g.remove_node(node)
        
        if nodes_to_remove == []:
            finished = True

    return g
    

def create_street_level(g, street_path):
    #for each edge in g (the water layer), we create two edges that surround it
    h = nx.Graph()
    h = h.to_undirected()
    vertical_edges = get_vertical_edges(g)
    horizontal_edges = get_horizontal_edges(g)
    offs = 0.2 #distance between water pipe and the street edges
    
    #create street borders around vertical edges
    for edge in vertical_edges: #the two nodes have the same x
        if edge[0][1] > edge[1][1]:
            #add edge on the left
            u = (edge[0][0]-offs, edge[0][1]-offs)
            v = (edge[1][0]-offs, edge[1][1]+offs)
            h.add_node(u)
            h.add_node(v)
            h.add_edge(u, v)
            #add edge on the right
            u = (edge[0][0]+offs, edge[0][1]-offs)
            v = (edge[1][0]+offs, edge[1][1]+offs)
            h.add_node(u)
            h.add_node(v)
            h.add_edge(u, v)
        else:
            #add edge on the left
            u = (edge[1][0]-offs, edge[1][1]-offs)
            v = (edge[0][0]-offs, edge[0][1]+offs)
            h.add_node(u)
            h.add_node(v)
            h.add_edge(u, v)
            #add edge on the right
            u = (edge[1][0]+offs, edge[1][1]-offs)
            v = (edge[0][0]+offs, edge[0][1]+offs)
            h.add_node(u)
            h.add_node(v)
            h.add_edge(u, v)
    
    #create street borders around horizontal edges
    for edge in horizontal_edges: #the two nodes have the same y
        if edge[0][0] < edge[1][0]:
            #add top edge
            u = (edge[0][0]+offs, edge[0][1]+offs)
            v = (edge[1][0]-offs, edge[1][1]+offs)
            h.add_node(u)
            h.add_node(v)
            h.add_edge(u, v)
            #add bottom edge
            u = (edge[0][0]+offs, edge[0][1]-offs)
            v = (edge[1][0]-offs, edge[1][1]-offs)
            h.add_node(u)
            h.add_node(v)
            h.add_edge(u, v)
        else:
            #add top edge
            u = (edge[0][0]-offs, edge[0][1]+offs)
            v = (edge[1][0]+offs, edge[1][1]+offs)
            h.add_node(u)
            h.add_node(v)
            h.add_edge(u, v)
            #add bottom edge
            u = (edge[0][0]-offs, edge[0][1]-offs)
            v = (edge[1][0]+offs, edge[1][1]-offs)
            h.add_node(u)
            h.add_node(v)
            h.add_edge(u, v)
    
    
    #Now we have to fill some of the gaps in the new edges that we just created
    #(there are gaps due to the fact that they are shorter than the water edges
    water_nodes = [] #contains nodes with degree 2 or 3
    for node in g.nodes():
        degree = nx.degree(g, node)
        if degree == 2 or degree == 3:
            water_nodes.append(node)
    street_nodes = [] #contains nodes with degree 2 or 3
    for node in h.nodes():
        degree = nx.degree(h, node)
        if degree == 1:
            street_nodes.append(node)
    
    print(water_nodes)
    print(street_nodes)
    for water_node in water_nodes:
        surrounding_nodes = []
        for street_node in street_nodes:
            #if it is close to the water_node
            if (abs(float(street_node[0]) - float(water_node[0])) <= offs+offs/10) and (abs(float(street_node[1]) - float(water_node[1])) <= offs+offs/10):
                surrounding_nodes.append(street_node) #add it to the surrounding nodes
                if len(surrounding_nodes) == 2: #if we've found at least two surrounding nodes
                    break #no need to keep looking, there can only be two in the scenarios we're considering
        if len(surrounding_nodes) == 2:
            if surrounding_nodes[0][0] != surrounding_nodes[1][0] and surrounding_nodes[0][1] != surrounding_nodes[1][1]: #they don't share any coordinate
                if nx.edges(h, (surrounding_nodes[0][0], surrounding_nodes[1][1])) == []: #there is no node in this position
                    new_node = (surrounding_nodes[0][0], surrounding_nodes[1][1]) #this will be the new node
                    h.add_node(new_node) #add the node
                    h.add_edge(new_node, (surrounding_nodes[0][0], surrounding_nodes[0][1]))
                    h.add_edge(new_node, (surrounding_nodes[1][0], surrounding_nodes[1][1]))
                    
                else:
                    new_node = (surrounding_nodes[1][0], surrounding_nodes[0][1])
                    h.add_node(new_node)
                    h.add_edge(new_node, (surrounding_nodes[0][0], surrounding_nodes[0][1]))
                    h.add_edge(new_node, (surrounding_nodes[1][0], surrounding_nodes[1][1]))
            else: #the two nodes have the same x or y coor
                h.add_edge((surrounding_nodes[0][0], surrounding_nodes[0][1]), (surrounding_nodes[1][0], surrounding_nodes[1][1])) #connect them
            
    
    
    h = remove_meaningless_nodes(h)
    network_to_shp(h, street_path)
        
    


def main():
    path = os.getcwd()
    
    # generate a complete, undirected graph
    g = nx.complete_graph(130)  # fast_gnp_random_graph(100, 0.6)
    g = g.to_undirected()

    # coordinates of nodes will be numbers drawn from this list
    possible_coordinates = list(range(100))


    mapping = {}
    i = 0
    for d in g.nodes_iter(data=True):
        x = random.choice(possible_coordinates)
        y = random.choice(possible_coordinates)
        coord = (x, y)
        nx.set_node_attributes(g, 'coord', {d[0]: coord})
        mapping[i] = coord
        i += 1
    g = nx.relabel_nodes(g, mapping)

    # add loops as well as the edges that aren't vertical or horizontal to a list of
    # edges that we want to remove
    edges_to_be_removed = []
    for e in g.edges_iter():
        node_start_x = e[0][0]
        node_start_y = e[0][1]
        node_end_x = e[1][0]
        node_end_y = e[1][1]
        if (node_start_x != node_end_x and node_start_y != node_end_y) or (node_start_x==node_end_x and node_start_y==node_end_y):
            edges_to_be_removed.append((e[0], e[1]))

    # remove the edges in the list
    for edge in edges_to_be_removed:
        g.remove_edge(edge[0], edge[1])

    g = remove_single_nodes(g, None)
    g = add_nodes_crossings(g)
    g = clean_overlapping_edges(g)
    

    network_to_shp(g, path + '/OrthogonalGraphs/Complete/')
    gcopy = nx.read_shp(path + '/OrthogonalGraphs/Complete/', True)
    g = remove_double_edges(g, gcopy)
    
    
    edges_to_be_removed = []
    #remove self-loops
    for e in g.edges_iter():
        node_start_x = e[0][0]
        node_start_y = e[0][1]
        node_end_x = e[1][0]
        node_end_y = e[1][1]
        if node_start_x==node_end_x and node_start_y==node_end_y:
            edges_to_be_removed.append((e[0], e[1]))

    # remove the edges in the list
    for edge in edges_to_be_removed:
        g.remove_edge(edge[0], edge[1])
    
    g = remove_single_nodes(g, None)   
    g = remove_meaningless_nodes(g)

    network_to_shp(g, path + '/OrthogonalGraphs/Complete/')
    
    g_shp = ogr.Open(path + '/OrthogonalGraphs/Complete/edges.shp', 1)
    set_diameters(g, g_shp)
    
    create_street_level(g, path + '/OrthogonalGraphs/Street_Level/')


    # NOW WE REMOVE SOME RANDOM EDGES (OR SPLIT THEM)
    # AND SAVE THE SHAPEFILES IN THE
    # MISSING_EDGES AND SPLIT_EDGES FOLDERS

    # Randomly choose 10% of edges that we want to remove or split
    edges_to_be_removed = []
    #to_be_split = []
    for e in g.edges_iter():
        if random.randint(1, 10) == 1:
            edges_to_be_removed.append((e[0], e[1]))
            #to_be_split.append((e[0], e[1]))

    #now find the nodes that will need to

    #Copy the shapefiles in the Complete folder to the Missing_Edges folder
    src_files = os.listdir(path + '/OrthogonalGraphs/Complete/')
    for file_name in src_files:
        full_file_name = os.path.join(path + '/OrthogonalGraphs/Complete/', file_name)
        if (os.path.isfile(full_file_name)):
            copyfile(full_file_name, path + '/OrthogonalGraphs/Missing_Edges/'+file_name)

    missing_edges_graph = g.copy()
    incomplete_nodes_shapefile = ogr.Open(path + '/OrthogonalGraphs/Missing_Edges/nodes.shp', 1)
    incomplete_nodes_layer = incomplete_nodes_shapefile.GetLayerByIndex(0)
    incomplete_edges_shapefile = ogr.Open(path + '/OrthogonalGraphs/Missing_Edges/edges.shp', 1)
    incomplete_edges_layer = incomplete_edges_shapefile.GetLayerByIndex(0)
    #split_edges_graph = g.copy()
    # Remove the edges
    for edge in edges_to_be_removed:
        missing_edges_graph.remove_edge(edge[0], edge[1])
        for edgefeature in range(incomplete_edges_layer.GetFeatureCount()):
            current_feature = incomplete_edges_layer.GetFeature(edgefeature)
            if current_feature is not None:
                x_start = current_feature.GetFieldAsDouble(1)
                y_start = current_feature.GetFieldAsDouble(2)
                x_end = current_feature.GetFieldAsDouble(3)
                y_end = current_feature.GetFieldAsDouble(4)
                if edge[0][0] == x_start and edge[0][1] == y_start and edge[1][0] == x_end and edge[1][1] == y_end:
                    incomplete_edges_layer.DeleteFeature(edgefeature)
                for nodefeature in range(incomplete_nodes_layer.GetFeatureCount()):
                    current_feature = incomplete_nodes_layer.GetFeature(nodefeature)
                    if current_feature is not None:
                        if edge[0][0] == current_feature.GetFieldAsDouble(1) and edge[0][1] == current_feature.GetFieldAsDouble(2):
                            current_feature.SetField("Flagged", 1)
                            incomplete_nodes_layer.SetFeature(current_feature)
                            incomplete_nodes_layer.SyncToDisk()
                        if edge[1][0] == current_feature.GetFieldAsDouble(1) and edge[1][1] == current_feature.GetFieldAsDouble(2):
                            current_feature.SetField("Flagged", 1)
                            incomplete_nodes_layer.SetFeature(current_feature)
                            incomplete_nodes_layer.SyncToDisk()
                pass
    incomplete_edges_layer.SyncToDisk()

    missing_edges_graph = remove_single_nodes(missing_edges_graph, incomplete_nodes_layer)

    #split_edges_graph = split_edges(split_edges_graph, to_be_split)
    #split_edges_graph = remove_single_nodes(split_edges_graph)

    #nx.write_shp(missing_edges_graph, path + '/OrthogonalGraphs/Missing_Edges/')
    #nx.write_shp(split_edges_graph, path + '/OrthogonalGraphs/Split_Edges/')

    print("ok")


if __name__ == '__main__':
    main()
