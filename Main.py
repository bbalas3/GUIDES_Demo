import osgeo
import osgeo.ogr as ogr
import sys
import os
import time
import networkx as nx
from shutil import copyfile
import Flagger
import csv

path = os.getcwd()

path_infrastructure = path+'/Infrastructure/'
path_pipes = path+'/Infrastructure/PipesConverted/'

 #Copy the content of PipesConverted into /Clean, so that
 #we can work in this new folder and clean the original maps while preserving them
src_files = os.listdir(path_pipes)
for file_name in src_files:
    full_file_name = os.path.join(path_pipes, file_name)
    if (os.path.isfile(full_file_name)):
        copyfile(full_file_name, path_pipes+"Clean/"+"clean_"+file_name)
 
 
 
 
#enrich the attributes of the edges so that we can know the start and end nodes for each of them
edges_shapefile = ogr.Open(path_pipes+"Clean/clean_Edges_new-C-WATR-UNDR.shp",1 )
edges_layer = edges_shapefile.GetLayerByIndex(0)
 
edge_field_x_start = ogr.FieldDefn()
edge_field_x_start.SetName("x_start")
edge_field_x_start.SetType(ogr.OFTReal)
edges_layer.CreateField(edge_field_x_start)
 
edge_field = ogr.FieldDefn()
edge_field.SetName("y_start")
edge_field.SetType(ogr.OFTReal)
edges_layer.CreateField(edge_field)
 
edge_field = ogr.FieldDefn()
edge_field.SetName("x_end")
edge_field.SetType(ogr.OFTReal)
edges_layer.CreateField(edge_field)
 
edge_field = ogr.FieldDefn()
edge_field.SetName("y_end")
edge_field.SetType(ogr.OFTReal)
edges_layer.CreateField(edge_field)
 
edges_shapefile.SyncToDisk()
 
#we create a new layer
sp_ref = edges_shapefile.GetLayerByIndex(0).GetSpatialRef()
geom_type = edges_shapefile.GetLayerByIndex(0).GetGeomType()
edges_shapefile.CreateLayer("temp", sp_ref, geom_type)
new_layer = edges_shapefile.GetLayerByIndex(1)
 
 
 #copy fields to the new layer
oldLayerDefn = edges_layer.GetLayerDefn()
for i in range(0, oldLayerDefn.GetFieldCount()):
    fieldDefn = oldLayerDefn.GetFieldDefn(i)
    new_layer.CreateField(fieldDefn)
 
with open(path_pipes+'Clean/clean_Edgelist_new-C-WATR-UNDR.csv', 'r') as csvfile:
    reader = csv.reader(csvfile)
    next(reader, None)
    i = 0
    for ring in reader:
        edge_feature = edges_layer.GetFeature(i)
        edge_feature.SetField("x_start", ring[0])# float(ring[0]))
        edge_feature.SetField("y_start", ring[1])
        edge_feature.SetField("x_end", ring[2])
        edge_feature.SetField("y_end", ring[3])
        new_layer.CreateFeature(edge_feature)
        edges_layer.SyncToDisk()
        new_layer.SyncToDisk()
        i += 1
edges_shapefile.DeleteLayer(0)
edges_shapefile.SyncToDisk()
new_layer = edges_shapefile.GetLayerByIndex(0)
 
src_files = os.listdir(path_pipes+"Clean/")
for file_name in src_files:
    if "temp" in file_name:
        full_file_name = os.path.join(path_pipes+"Clean/", file_name)
        os.rename(full_file_name, path_pipes+"Clean/clean_Edges_new-C-WATR-UNDR"+file_name[-4:])
 
 
Flagger.clean_duplicate_nodes()
 
#Copy the clean shapefiles to the NoLoops folder, and rename them
src_files = os.listdir(path_pipes+"Clean/")
for file_name in src_files:
    full_file_name = os.path.join(path_pipes+"Clean/", file_name)
    if (os.path.isfile(full_file_name)):
        copyfile(full_file_name, path_pipes+"NoLoops/"+"noloops_"+file_name[6:])
 
Flagger.replace_loops()
print ("finished replacing loops")

#Copy the noloops shapefiles to the Flagged folder, and rename them
src_files = os.listdir(path_pipes+"NoLoops/")
for file_name in src_files:
    full_file_name = os.path.join(path_pipes+"NoLoops/", file_name)
    if (os.path.isfile(full_file_name)) and "~" not in full_file_name:
        copyfile(full_file_name, path_pipes+"NoJoints/"+"nojoints_"+file_name[8:])


Flagger.replace_T_shapes()

#Copy the noloops shapefiles to the Flagged folder, and rename them
src_files = os.listdir(path_pipes+"NoJoints/")
for file_name in src_files:
    full_file_name = os.path.join(path_pipes+"NoJoints/", file_name)
    if (os.path.isfile(full_file_name)) and "~" not in full_file_name:
        copyfile(full_file_name, path_pipes+"Flagged/"+"flagged_"+file_name[9:])

Flagger.flag()



print ("ok")
